resource "aws_eks_cluster" "eks_cluster" {
  name     = var.eks_b_cluster_name
  role_arn = "arn:aws:iam::DesChiffres:role/EKSrole"

  vpc_config {
    subnet_ids = [aws_subnet.eks_subnet_a.id, aws_subnet.eks_subnet_b.id, aws_subnet.eks_subnet_c.id,
                aws_subnet.eks_public_subnet_a.id,aws_subnet.eks_public_subnet_b.id, aws_subnet.eks_public_subnet_c.id]
    security_group_ids = [aws_security_group.sec_cluster.id]

    endpoint_private_access = true
    endpoint_public_access = false 
  }

}

resource "aws_eks_node_group" "staging_eks_group" {
  cluster_name    = aws_eks_cluster.eks_cluster.name
  node_group_name = "eks-group"
  node_role_arn   =  "arn:aws:iam::DesChiffres:role/eks-autoscaling-group" 
  subnet_ids      = [aws_subnet.eks_subnet_a.id, aws_subnet.eks_subnet_b.id, aws_subnet.eks_subnet_c.id] 

  instance_types = ["t3.medium"] 

  scaling_config {
    desired_size = 2
    max_size     = 3
    min_size     = 1
  }

  remote_access {
    ec2_ssh_key = data.terraform_remote_state.ssh_keys.outputs.key_name
    source_security_group_ids = [aws_security_group.sec_group.id] 
  }

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }
}


resource "aws_elasticsearch_domain" "elasticsearch" {
  domain_name           = var.es_domain_name
  elasticsearch_version = var.es_version

  cluster_config {
    instance_count = 3
    instance_type = "r5.large.elasticsearch"
  }

  vpc_options {
    subnet_ids = [
      aws_subnet.subnet.id
    ]

    security_group_ids = [ aws_security_group.secgroup.id ]
  }

  advanced_security_options {
    enabled = true
    internal_user_database_enabled = true
    master_user_options {
      master_user_name = var.master_user
      master_user_password = var.master_password
    }
  }
  
  ebs_options {
      ebs_enabled = true
      volume_size = 100
  }

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }


access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": {
              "AWS":[
                 "*"
              ]
            },
            "Effect": "Allow",
            "Resource": "arn:aws:es:${var.region}:${var.aws_caller_identity}:domain/${var.es_domain_name}/*"
        }
    ]
}
CONFIG

  node_to_node_encryption {
    enabled = true
  }
  encrypt_at_rest {
    enabled = true
  }

  domain_endpoint_options {
    enforce_https = true
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }

}

output "elasticsearch_endpoint" {
  value = aws_elasticsearch_domain.bastion_elastic.endpoint
}

output "endpoint_eks" {
  value = aws_eks_cluster.eks_cluster.endpoint
}
output "certificate_authority_eks" {
  value = aws_eks_cluster.eks_cluster.certificate_authority[0].data
}
