resource "aws_internet_gateway" "lab-internet-gw" {
  vpc_id =  aws_vpc.lab.id
  tags = {
    Name = "lab-internet-gw"
    Environement = "lab"
  }
}

resource "aws_ecs_cluster" "archi1" {
  name = "archi1"
}

resource "aws_ecs_task_definition" "archi1" {
  family = "archi1"
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  execution_role_arn = "arn:aws:iam:::role/ecsTaskExecutionRole"
  cpu       = 256
  memory    = 512
  container_definitions = jsonencode([
    {
      name      = "archi1"
      image     = ".dkr.ecr.eu-west-3.amazonaws.com/monImage:1.0.449"
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 8080
          hostPort      = 8080
        }
      ]
    }])
}

resource "aws_ecs_service" "archi1" {
  name            = "archi1"
  cluster         = aws_ecs_cluster.archi1.id
  task_definition = aws_ecs_task_definition.archi1.arn
  desired_count   = 1

  launch_type = "FARGATE"

  network_configuration {
    subnets = [" subnet-08","subnet-0b"]
    security_groups = ["sg-0c"]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.archi1.arn
    container_name   = "archi1"
    container_port   = 8080
  }
}

resource "aws_lb_target_group" "archi1" {
  port     = 8080
  protocol = "HTTP"
  target_type  = "ip"
  vpc_id   = aws_vpc.lab.id
}

resource "aws_lb" "archi1" {
  name               = "archi1"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg.id]
  subnets            = [aws_subnet.public_1.id,aws_subnet.public_2.id]
  depends_on         = [aws_internet_gateway.lab-internet-gw]
  
  access_logs {
    bucket  = aws_s3_bucket.AWS_config_lab.bucket
    prefix  = "archi1-lb"
    enabled = true
  }

}

resource "aws_lb_listener" "archi1" {
  load_balancer_arn = aws_lb.archi1.arn
  port              = "443"
  protocol          = "HTTPS"

  certificate_arn   = "arn:aws:acm:eu-west-3::certificate/"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.archi1.arn
  }
}

resource "aws_s3_bucket" "AWS_config_lab" {
  bucket = "doxense-awsconfiglab"
  acl    = "private" 

  tags = {
    Name        = "doxense-awsconfiglab"
    env         = "lab"
  }
}


# Ca c'est horrible mais j'ai voulu faire vite
resource "aws_s3_bucket_policy" "AWS_config_lab_policy" {
  bucket = aws_s3_bucket.AWS_config_lab.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:PutObject"
        Resource = [aws_s3_bucket.AWS_config_lab.arn, "${aws_s3_bucket.AWS_config_lab.arn}/*"]
      },
    ]
  })
}